package com.example.robot;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

public class DetailsActivity extends AppCompatActivity {
    Button activate;
    EditText input;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        activate    = findViewById(R.id.button);
        input       = findViewById(R.id.editText);


        activate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String lines[] = string.split("\\r?\\n");
                String[] text = String.valueOf(input.getText()).toUpperCase().split("\\r?\\n");

                Log.e("Final text", String.valueOf(text.length));
                int num = (text.length - 1)/2;
                Log.e("Number of Robots",String.valueOf(num));
                ArrayList<String> position = new ArrayList<>();
                ArrayList<String> path = new ArrayList<>();
                for(int i=1;i<text.length;i++){
                    if(i%2 == 0){
                        path.add(text[i]);
                    }else{
                        position.add(text[i]);
                    }
                }
                Global.robots = num;
                Global.maxSize = text[0].split(" ");
                Global.position = position;
                Global.path = path;
                Intent i = new Intent(getApplicationContext(),MainActivity.class);

                startActivity(i);
            }
        });
    }
}
