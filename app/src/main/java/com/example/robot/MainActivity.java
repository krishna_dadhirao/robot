package com.example.robot;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActionBar;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;


public class MainActivity extends AppCompatActivity {


    String [] magneticDirections = {"N","E","S","W"};
    int currentDirection = 0;
    int currentX = 0;
    int currentY = 0;
    int robotNum = 0;
    TextView numberRobots,robotsTv,posotionTv;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        Arrays.asList(magneticDirections).indexOf("N"); //pass index

        numberRobots    = findViewById(R.id.numberOfRobots);
        robotsTv        = findViewById(R.id.robotsTv);
        posotionTv      = findViewById(R.id.positionTv);

        numberRobots.setText("Number of Robots: "+Global.robots);
//        ActionBar actionBar = getActionBar();
//        actionBar.setHomeButtonEnabled(true);

//        getActionBar().setHomeButtonEnabled(true);

        for(int i=0;i<Global.robots;i++){
            robotNum = i+1;
            String [] currentPosition = Global.position.get(i).split(" ");
            currentX = Integer.parseInt(currentPosition[0]);
            currentY = Integer.parseInt(currentPosition[1]);
            currentDirection = Arrays.asList(magneticDirections).indexOf(currentPosition[2]);
            for(int j=0;j<Global.path.get(i).length();j++){
                changePath(Global.path.get(i).charAt(j));
            }
            robotsTv.append("\n\nRobot "+(i+1));
            posotionTv.append("\n\n"+(currentX)+" "+(currentY)+" "+magneticDirections[currentDirection]);
        }



    }





    public void onPause() {
        super.onPause();
    }


    // P - Left , R - Right, Q - 1 Step
    public void changePath(Character move){
        switch(move) {
            case 'P':
                currentDirection = currentDirection - 1;
                if(currentDirection < 0){
                    currentDirection = 3;
                }

                Log.d("Current Direction", (currentX)+" "+(currentY)+" "+magneticDirections[currentDirection]);
                break;
            case 'Q':
                updatePath();
                break;
            case 'R':
                currentDirection = currentDirection + 1;
                if(currentDirection > 3){
                    currentDirection = 0;
                }
                Log.d("Current Direction", (currentX)+" "+(currentY)+" "+magneticDirections[currentDirection]);
                break;

            default:

                break;
        }
    }

    public void updatePath(){
        switch (currentDirection){
            case 0:
                currentY = currentY + 1;
                if(currentY > Integer.parseInt(Global.maxSize[1])){
                    showToast("Robot "+robotNum+":"+"Out of Bound - Y Axis");
                }else{
                    Log.d("Current Direction", (currentX)+" "+(currentY)+" "+magneticDirections[currentDirection]);
                }

                break;
            case 1:
                currentX = currentX + 1;
                if(currentX > Integer.parseInt(Global.maxSize[0])){
                    showToast("Robot "+robotNum+":"+"Out of Bound - X Axis");
                }else{
                    Log.d("Current Direction", (currentX)+" "+(currentY)+" "+magneticDirections[currentDirection]);
                }

                break;
            case 2:
                currentY = currentY - 1;
                if(currentY < 0){
                    showToast("Robot "+robotNum+":"+"Out of Bound - Y Axis");
                }else{
                    Log.d("Current Direction", (currentX)+" "+(currentY)+" "+magneticDirections[currentDirection]);
                }
                break;
            case 3:
                currentX = currentX - 1;
                if(currentX < 0){
                    showToast("Robot "+robotNum+":"+"Out of Bound - X Axis");
                }else{
                    Log.d("Current Direction", (currentX)+" "+(currentY)+" "+magneticDirections[currentDirection]);
                }
                break;
            default:
                break;
        }



    }

    public void showToast(String message){
        Toast toast = Toast.makeText(getApplicationContext(),
                message,
                Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER,0,0);

        toast.show();
    }
}
